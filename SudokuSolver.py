from sys import argv as filename
from math import floor as floor

sudokuPlate = []
with open(filename[1]) as file:
	for line in file:
		sudokuPlate.append([int(i) for i in line.strip().split(',')])

def localBox(k): # returns zip with coordinates for specified box
	r = floor(k/3)*3
	c = (k-3*floor(k/3))*3
	row = (0+r,0+r,0+r,1+r,1+r,1+r,2+r,2+r,2+r)
	col = (0+c,1+c,2+c,0+c,1+c,2+c,0+c,1+c,2+c)
	return row, col

def checkRow(row,n): # check if n is in the column y
	for i in range(0,9):
		if n == sudokuPlate[row][i]:
			return False
	return True

def checkCol(col,n): # check if n is in the column x
	for i in range(0,9):
		if n == sudokuPlate[i][col]:
			return False
	return True

notSolved = True
for k in range(3):
	for box in range(9):
		r,c = localBox(box) # current box
		for j in range(1,10):
			if len([(row,col) for row,col in zip(r,c) if sudokuPlate[row][col] == j]) == 0:
				round = [(j,(row,col)) for row, col in zip(r,c) if checkCol(col,j) and checkRow(row,j)]
				print(round)
				if len(round) == 1:
					print('exchange', round)
					sudokuPlate[round[0][1][0]][round[0][1][1]] = round[0][0]
for x in sudokuPlate:
	print(x)